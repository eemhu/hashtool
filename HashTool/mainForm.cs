﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace HashTool
{
    public partial class mainForm : Form
    {
        const string programVersion = "0.0.3";

        public mainForm()
        {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            this.Text = "HashTool ver " + programVersion;
            cbHashType.SelectedIndex = 0;
        }

        static string computeMD5(string filename)
        {
            using (MD5 m5hash = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    byte[] bytes = m5hash.ComputeHash(stream);
                    return BitConverter.ToString(bytes).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        static string computeSHA1(string filename)
        {
            using (SHA1 s1hash = SHA1.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    byte[] bytes = s1hash.ComputeHash(stream);
                    return BitConverter.ToString(bytes).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        static string computeSHA256(string filename)
        {
            using (SHA256 s256hash = SHA256.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    byte[] bytes = s256hash.ComputeHash(stream);
                    return BitConverter.ToString(bytes).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        static string computeSHA384(string filename)
        {
            using (SHA384 s384hash = SHA384.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    byte[] bytes = s384hash.ComputeHash(stream);
                    return BitConverter.ToString(bytes).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        static string computeSHA512(string filename)
        {
            using (SHA512 s512hash = SHA512.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    byte[] bytes = s512hash.ComputeHash(stream);
                    return BitConverter.ToString(bytes).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select the file from which the hash will be calculated";
            ofd.Filter = "All files (*.*)|*.*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                tbFilename.Text = ofd.FileName;
            }
        }

        private void btnCompare_Click(object sender, EventArgs e)
        {
            if (!File.Exists(tbFilename.Text))
            {
                MessageBox.Show("File doesn't exist, check path.");
                return;
            }

            if (cbHashType.Text != null)
            {
                string hash;
                string comp = tbCompare.Text;

                if (cbHashType.Text == "MD5")
                {
                    hash = computeMD5(tbFilename.Text);
                }
                else if (cbHashType.Text == "SHA1")
                {
                    hash = computeSHA1(tbFilename.Text);
                }
                else if (cbHashType.Text == "SHA256")
                {
                    hash = computeSHA256(tbFilename.Text);
                }
                else if (cbHashType.Text == "SHA384")
                {
                    hash = computeSHA384(tbFilename.Text);
                }
                else
                {
                    hash = computeSHA512(tbFilename.Text);
                }

                if (comp.Trim(' ').ToLowerInvariant().Equals(hash))
                {
                    MessageBox.Show("The file is valid.","Results",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
                else
                {
                    if (cbCopy.Checked)
                    {
                        Clipboard.SetText(hash);
                    }

                    MessageBox.Show("The file is INVALID!" + Environment.NewLine + "File hash: " + hash + Environment.NewLine + "Given hash: " + comp,"Results",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Please select hash type first.");
            }
            
        }

        private void cbHashType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
